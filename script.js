var fs        = require('fs');
var XmlStream = require('xml-stream');
var readdirp = require('readdirp')
  , path = require('path')
  , es = require('event-stream');

// print out all JavaScript files along with their size

var readStream = readdirp({ root: path.join(__dirname, 'testing'), fileFilter: [ '*.tsl', '*.xdt' ] });
readStream
  .on('warn', function (err) { 
    console.error('non-fatal error', err); 
    // optionally call readStream.destroy() here in order to abort and cause 'close' to be emitted
  })
  .on('error', function (err) { console.error('fatal error', err); })
  .pipe(es.mapSync(function(entry){
  	return findTag(entry);
  }))
  .pipe(es.stringify())
  .pipe(process.stdout);


var findTag = function(entry){
	// console.log("inside findTag "+entry.fullPath)
	var stream=fs.createReadStream(entry.fullPath);
	var xml = new XmlStream(stream);

	xml.on('startElement: NormalParagraphStyle', function(item) {
	  if(item.$.font_name == '/Times') 
	  	// console.log(item.$.font_name + ' '+'Bold');
	  console.log(item);
	  // return item;

	})

	xml.on('startElement: PF_Para_Base', function(item) {
	  if( item.$.font_name == '/Times') 
	  	// console.log(item.$.font_name + ' '+'Bold');
	  console.log(item);
	  // return item;
	})
}