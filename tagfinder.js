var fs        = require('fs');
var XmlStream = require('xml-stream');
var path = require('path');
var list = [];

// var folder = ;
var p = './search4fonts'
fs.readdir(p, function (err, files) {
    if (err) {
        console.log('Folder not found');
        return;
    }

    // files.map(function (file) {
    //     return path.join(p, file);
    // }).filter(function (file) {
    //     return fs.statSync(file).isFile();
    // })
    files.forEach(function (file) {
    	if(path.extname(file) == '.xdt' || path.extname(file) == '.tsl') 
    		if(fs.statSync(path.join(p, file)).isFile())
				findTag({name: file, fullPath: path.join(p, file), dir: p});
    });
});

var folder = p+'/Area Templates';
fs.readdir(folder, function (err, files) {
    if (err) {
        console.log('Area Folder not found');
        return;
    }

    // files.map(function (file) {
    //     return path.join(folder, file);
    // }).filter(function (file) {
    //     return fs.statSync(file).isFile();
    // })
    files.forEach(function (file) {
    	if(path.extname(file) == '.xat') 
    		if(fs.statSync(path.join(folder, file)).isFile()){
				findTag({name: file, fullPath: path.join(folder, file), dir: folder});
				// console.log("Done: "+file);
    		}
    });
});

var findTag = function(entry){
	// console.log("inside findTag "+entry.fullPath)
	var stream=fs.createReadStream(entry.fullPath);
	var xml = new XmlStream(stream);

	xml.on('startElement: NormalParagraphStyle', function(item) {
		// console.log("calling checker")
	  checker(item, entry);
	})

	xml.on('startElement: PF_Para_Base', function(item) {
	  checker(item, entry);
	})
}

var checker = function(item, entry){
	// console.log("inside checker")
	var x = item.$;
	if(x.font_name){
		if(x.bold){
			if(x.bold=='true'){
				if(x.italic){
					if(x.italic== 'true') {
						var m = entry.dir+ ', '+entry.name+', '+x.font_name +' Bold Italic';
						if(list.indexOf(m) == -1){
							list.push(m);
							console.log(m);
						}
						// console.log(entry.dir+ ', '+entry.name+', '+x.font_name +' Bold Italic');
					}
				}
				else{
					var m = entry.dir+ ', '+entry.name+', '+x.font_name +' Bold';
					if(list.indexOf(m) == -1){
						list.push(m);
						console.log(m);
					}
					// console.log(entry.dir+ ', '+entry.name+', '+x.font_name +' Bold');
					return;
				}
			}
			else{
				if(x.italic){
					if(x.italic== 'true'){ 
						var m = entry.dir+ ', '+entry.name+', '+x.font_name +' Italic';
						if(list.indexOf(m) == -1){
							list.push(m);
							console.log(m);
						}
						// console.log(entry.dir+ ', '+entry.name+', '+x.font_name +' Italic');
						return;
					}
				}
				var m = entry.dir+ ', '+entry.name+', '+x.font_name;
				if(list.indexOf(m) == -1){
					list.push(m);
					console.log(m);
				}
				// console.log(entry.dir+ ', '+entry.name+', '+x.font_name);
				return;
			}
		}
		else{
			if(x.italic){
				if(x.italic== 'true') {
					var m = entry.dir+ ', '+entry.name+', '+x.font_name +' Italic';
					if(list.indexOf(m) == -1){
						list.push(m);
						console.log(m);
					}
					// console.log(entry.dir+ ', '+entry.name+', '+x.font_name +' Italic');
					return;
				}
			}
			var m = entry.dir+ ', '+entry.name+', '+x.font_name;
			if(list.indexOf(m) == -1){
				list.push(m);
				console.log(m);
			}
			// console.log(entry.dir+ ', '+entry.name+', '+x.font_name);
			return;			
		}
	}
}

// var files = fs.readdirSync(folder);
// // console.log(files);
// files.forEach(function(file){
// 	// console.log(path.extname(file))
// 	if(path.extname(file) == '.tsl' | path.extname(file) == '.xdt') 
// 		findTag({fullPath: path.join(__dirname, folder, file)});
// })

// var area = fs.readdirSync(path.join(__dirname,folder, 'Area Templates'));
// area.forEach(function(file){
// 	// console.log(path.extname(file))
// 	if(path.extname(file) == '.xat') 
// 		findTag({fullPath: path.join(__dirname,folder, 'Area Templates', file)});
// })